<?php
/**
 * Created by Nivanka Fonseka (nivanka@silverstripers.com).
 * User: nivankafonseka
 * Date: 6/12/15
 * Time: 9:15 AM
 * To change this template use File | Settings | File Templates.
 */

class LandingPage extends Page {

	private static $db = array(
		'Description'		=> 'HTMLText',
		'DescriptionColor'	=> 'Varchar',
		'DescriptionBG'		=> 'Varchar',
		'LogoBackground'	=> 'Varchar'
	);

	private static $has_one = array(
		'MainImage'			=> 'Image',
		'LogoImage'			=> 'Image'
	);

	public function getCMSFields(){
		$fields = parent::getCMSFields();

		$fields->removeByName('Content');

		$fields->addFieldsToTab('Root.Main', array(
			UploadField::create('MainImage'),
			HtmlEditorField::create('Description'),
			TextField::create('DescriptionColor'),
			TextField::create('DescriptionBG'),
			UploadField::create('LogoImage')->setTitle('Bottom logo image'),
			TextField::create('LogoBackground')
		));

		return $fields;
	}

}

class LandingPage_Controller extends Page_Controller {




}