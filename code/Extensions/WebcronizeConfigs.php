<?php
/**
 * Created by Nivanka Fonseka (nivanka@silverstripers.com).
 * User: nivankafonseka
 * Date: 7/6/15
 * Time: 6:44 PM
 * To change this template use File | Settings | File Templates.
 */

class WebcronizeConfigs extends DataExtension {

	private static $db = array(
		'FootNote'			=> 'HTMLVarchar(500)'
	);

	public function updateCMSFields(FieldList $fields){
		$fields->addFieldsToTab('Root.Settings.Site', array(
			HtmlEditorField::create('FootNote')->setRows(5)
		));
	}

} 