<?php
/**
 * Created by Nivanka Fonseka (nivanka@silverstripers.com).
 * User: nivankafonseka
 * Date: 7/7/15
 * Time: 6:21 PM
 * To change this template use File | Settings | File Templates.
 */

class UserFormsExtension extends DataExtension {

	private static $db = array(
		'SidebarContent'		=> 'HTMLText'
	);

	private static $has_one = array(
		'SidebarImage'			=> 'Image'
	);


	public function updateCMSFields(FieldList $fields){
		$fields->addFieldsToTab('Root.Sidebar', array(
			UploadField::create('SidebarImage'),
			HtmlEditorField::create('SidebarContent')
		));
	}

}